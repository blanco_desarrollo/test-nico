# Test para Nico #

## Descripción ##
Realizar una app que realice un login y muestre un listado de elementos (juegos en este caso)

Para esto, disponemos de una API con los siguientes endpoints (http://138.197.196.64:3002)

### Login ###
Endpoint
```
POST /login
```
Body
```
#!json
{
    "email": "test@test.com",
    "password": "1234"
}
```
Response 
```
#!json
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ODEzNzEx...",
  "profile": {
        "lastLoginAt": "2017-11-27T16:26:01.280Z",
        "_id": "5a1c3c10191050480272a53c",
        "createdBy": "59b2ba3633ad77a4789c6f20",
        "phone": "1234567890",
        "address": "My Home",
        "birthday": "2017-11-01T03:00:00.000Z",
        "nickName": "test@test.com",
        "active": true,
        "role": {
            "_id": "59b2b89f33ad77a4789c6eae",
            "description": "Administrador",
            "name": "admin",
            "updatedAt": "2017-09-08T00:00:00.590Z",
            "createdAt": "2017-09-08T00:00:00.590Z"
        },
        "email": "test@test.com",
        "name": "Test",
        "lastName": "Test",
        "firstName": "Test",
        "__v": 0,
        "updatedAt": "2017-11-27T16:23:44.003Z",
        "createdAt": "2017-11-27T16:23:44.003Z",
        "sessionStatus": "offline"
    }
}
```

## Listado de Juegos

Endpoint
```
GET /game
```
Header
```
x-access-token: <ADMIN-TOKEN>
```
Return
```
#!json
{
    "docs": [
        {
            "_id": "59b991e34adc6152a10f0e3c",
            "endDate": "2017-10-31T00:00:00.000Z",
            "password": "$2a$10$FxX6tpJZyMpHzJUA9LIYRuILe9SS6MIhuOGxYSNCPCOYtpp.SaXz6",
            "active": true,
            "name": "Octubre - 01",
            "updatedAt": "2017-09-13T20:15:31.751Z",
            "createdAt": "2017-09-13T20:15:31.751Z",
            "createdBy": "111111111111111111111111",
            "__v": 0
        },
        {
            "_id": "59b994e5dde215541ec61353",
            "endDate": "2017-10-31T00:00:00.000Z",
            "password": "$2a$10$CNbhYkiGKjF.kegYcM5zUOls/M/Lvso5/UfewkvAhVw928PKdi0Ra",
            "active": true,
            "name": "Octubre - 02",
            "updatedAt": "2017-09-13T20:28:21.266Z",
            "createdAt": "2017-09-13T20:28:21.266Z",
            "createdBy": "111111111111111111111111",
            "__v": 0
        }
    ],
    limit: 10,
    offset: 0,
    total: 2
]
```

## Entregable ##
Una App que realice el login utilizando las siguientes credenciales
```
- email: test@test.com,
- password": 1234
```

Agregar una segunda pantalla que muestre un listado de juegos con la siguiente info:
```
- Nombre del juego (name)
- Fecha de finalizacion (endDate)
- Estado del juego (active)
```
